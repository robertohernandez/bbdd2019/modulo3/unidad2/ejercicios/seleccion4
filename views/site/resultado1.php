<?php
use yii\widgets\ListView;

?>
<div class="row">
<?= ListView::widget([
    'dataProvider' => $resultados,
    'itemView' => '_post',
    'layout'=>"{summary}\n{pager}\n{items}",
    
]);
?>
</div>

