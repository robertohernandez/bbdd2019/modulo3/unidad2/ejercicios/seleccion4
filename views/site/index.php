<?php
use yii\helpers\Html;

/* @var $this yii\web\View */

$this->title = 'Consultas de Seleccion 3';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>Consultas de seleccion 3</h1>

        <p class="lead">Modulo 3 - unidad 2</p>
        
    </div>

    <div class="body-content">
        <div class="row">
            <div class="col-sm-6 col-md-4">
                <div class="thumbnail">
                    <div class="caption">
                        <h3>Consulta 1.1 Active Record</h3>
                        <p>Listar nombre y edad de los ciclistas que han ganado etapas</p>
                        <p>
                            <?= Html::a('Active Record', ['site/consulta1a'], ['class' => 'btn btn-primary']) ?>
                            <?= Html::a('Dao', ['site/consulta1'], ['class' => 'btn btn-default']) ?>
                        </p>
                    </div>
                  </div>
                </div>
            
            <div class="col-sm-6 col-md-4">
                <div class="thumbnail">
                    <div class="caption">
                        <h3>Consulta 1.2</h3>
                        <p>Listar nombre y edad de los ciclistas que han ganado puertos</p>
                        <p>
                            <?= Html::a('Active Record', ['site/consulta2a'], ['class' => 'btn btn-primary']) ?>
                            <?= Html::a('Dao', ['site/consulta2'], ['class' => 'btn btn-default']) ?>
                        </p>
                    </div>
                  </div>
                </div>
            
              
            <div class="col-sm-6 col-md-4">
                <div class="thumbnail">
                    <div class="caption">
                        <h3>Consulta 1.3</h3>
                        <p>Listar nombre y edad de los ciclistas que han ganado puertos y etapas</p>
                        <p>
                            <?= Html::a('Active Record', ['site/consulta3a'], ['class' => 'btn btn-primary']) ?>
                            <?= Html::a('Dao', ['site/consulta3'], ['class' => 'btn btn-default']) ?>
                        </p>
                    </div>
                </div>
            </div>
            
            <div class="col-sm-6 col-md-4">
                <div class="thumbnail">
                    <div class="caption">
                        <h3>Consulta 1.4</h3>
                        <p>Director de los equipos que tengan ciclistas que hayan ganado alguna etapa</p>
                        <p>
                            <?= Html::a('Active Record', ['site/consulta4a'], ['class' => 'btn btn-primary']) ?>
                            <?= Html::a('Dao', ['site/consulta4'], ['class' => 'btn btn-default']) ?>
                        </p>
                    </div>
                </div>
            </div>
         <!------------------------------------------->   
         
             <div class="col-sm-6 col-md-4">
                <div class="thumbnail">
                    <div class="caption">
                        <h3>Consulta 1.5</h3>
                        <p>Dorsal y nombre de los ciclistas que han llevado algun maillot</p>
                        <p>
                            <?= Html::a('Active Record', ['site/consulta5a'], ['class' => 'btn btn-primary']) ?>
                            <?= Html::a('Dao', ['site/consulta5'], ['class' => 'btn btn-default']) ?>
                        </p>
                    </div>
                </div>
            </div>
   
            
              <div class="col-sm-6 col-md-4">
                <div class="thumbnail">
                    <div class="caption">
                        <h3>Consulta 1.6</h3>
                        <p>Listar Dorsal y nombre de los ciclistas que han llevado el maillot amarillo</p>
                        <p>
                            <?= Html::a('Active Record', ['site/consulta6a'], ['class' => 'btn btn-primary']) ?>
                            <?= Html::a('Dao', ['site/consulta6'], ['class' => 'btn btn-default']) ?>
                        </p>
                    </div>
                </div>
            </div>
         
            <div class="col-sm-6 col-md-4">
                <div class="thumbnail">
                    <div class="caption">
                        <h3>Consulta 1.7</h3>
                        <p>Listar Dorsal de los ciclistas que han llevado algun maillot y que han ganado etapas</p>
                        <p>
                            <?= Html::a('Active Record', ['site/consulta7a'], ['class' => 'btn btn-primary']) ?>
                            <?= Html::a('Dao', ['site/consulta7'], ['class' => 'btn btn-default']) ?>
                        </p>
                    </div>
                </div>
            </div>
            
            <div class="col-sm-6 col-md-4">
                <div class="thumbnail">
                    <div class="caption">
                        <h3>Consulta 1.8</h3>
                        <p>Listar numetapa de las etapas que tengan puertos</p>
                        <p>
                            <?= Html::a('Active Record', ['site/consulta8a'], ['class' => 'btn btn-primary']) ?>
                            <?= Html::a('Dao', ['site/consulta8'], ['class' => 'btn btn-default']) ?>
                        </p>
                    </div>
                </div>
            </div>
         
            <div class="col-sm-6 col-md-4">
                <div class="thumbnail">
                    <div class="caption">
                        <h3>Consulta 1.9</h3>
                        <p>Listar kms de las etapas que hayan ganado ciclistas de Banesto y con puertos</p>
                        <p>
                            <?= Html::a('Active Record', ['site/consulta9a'], ['class' => 'btn btn-primary']) ?>
                            <?= Html::a('Dao', ['site/consulta9'], ['class' => 'btn btn-default']) ?>
                        </p>
                    </div>
                </div>
            </div>
         
           <div class="col-sm-6 col-md-4">
                <div class="thumbnail">
                    <div class="caption">
                        <h3>Consulta 1.10</h3>
                        <p>Listar ciclistas que hayan ganado alguna etapa con puerto</p>
                        <p>
                            <?= Html::a('Active Record', ['site/consulta10a'], ['class' => 'btn btn-primary']) ?>
                            <?= Html::a('Dao', ['site/consulta10'], ['class' => 'btn btn-default']) ?>
                        </p>
                    </div>
                </div>
            </div>
         
            <div class="col-sm-6 col-md-4">
                <div class="thumbnail">
                    <div class="caption">
                        <h3>Consulta 1.11</h3>
                        <p>Nombre de los puertos ganados por ciclistas de banesto</p>
                        <p>
                            <?= Html::a('Active Record', ['site/consulta11a'], ['class' => 'btn btn-primary']) ?>
                            <?= Html::a('Dao', ['site/consulta11'], ['class' => 'btn btn-default']) ?>
                        </p>
                    </div>
                </div>
            </div>
           
          <div class="col-sm-6 col-md-4">
                <div class="thumbnail">
                    <div class="caption">
                        <h3>Consulta 1.12</h3>
                        <p>Etapas que tengan puerto que hayan sido ganados por ciclistas de banesto con mas de 200 km</p>
                        <p>
                            <?= Html::a('Active Record', ['site/consulta12a'], ['class' => 'btn btn-primary']) ?>
                            <?= Html::a('Dao', ['site/consulta12'], ['class' => 'btn btn-default']) ?>
                        </p>
                    </div>
                </div>
            </div>
         
        </div>
        

    </div>
</div>
