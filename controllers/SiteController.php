<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\Ciclista;
use app\models\Lleva;
use app\models\Puerto;
use app\models\Etapa;
use app\models\Equipo;
use app\models\Maillot;
use yii\data\ActiveDataProvider;
use yii\data\SqlDataProvider;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionCrud(){
        return $this->render("gestion");
    }   
     
    public function actionConsulta1(){
        //mediante DAO
//        $numero = yii::$app->db
//                ->createCommand('SELECT count(*) FROM ciclista')
//                ->queryScalar();
//                
           $dataProvider = new SqlDataProvider([
            'sql' => 'SELECT DISTINCT c.nombre,c.edad FROM etapa e JOIN ciclista c USING(dorsal)',
            //'totalCount' => $numero,
        ]);
        
        
        return $this->render('resultado',[
            "resultados"=>$dataProvider,
            "campos"=>['nombre','edad'],
            "titulo"=>"Consulta 1 con Dao",
            "enunciado"=>"Listar nombre y edad de los ciclistas que han ganado etapas",
            "sql"=>"SELECT DISTINCT c.nombre,c.edad FROM etapa e JOIN ciclista c USING(dorsal)",
        ]);
    }
    
    
    public function actionConsulta1a(){
        //mediante Active Record
        
         $dataProvider = new ActiveDataProvider([
             'query' => Ciclista::find()
                 ->innerJoin('etapa','ciclista.dorsal = Etapa.dorsal')
                 ->select("ciclista.nombre,ciclista.edad")->distinct(),
           'pagination' => [
                'pageSize' =>6,
            ],   
         ]);

        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['nombre','edad'],
            "titulo"=>"Consulta 1 con Active Record",
            "enunciado"=>"Listar nombre y edad de los ciclistas que han ganado etapas",
            "sql"=>"SELECT DISTINCT c.nombre,c.edad FROM etapa e JOIN ciclista c USING(dorsal)",
        ]);
      
    }
    
    public function actionConsulta2(){
        //mediante DAO         
           $dataProvider = new SqlDataProvider([
            'sql' => "SELECT nombre,edad FROM(
                            SELECT DISTINCT dorsal FROM puerto)c1 JOIN ciclista  USING(dorsal)",
        ]);
        
        
        return $this->render('resultado',[
            "resultados"=>$dataProvider,
            "campos"=>['nombre','edad'],
            "titulo"=>"Consulta 1.2 con Dao",
            "enunciado"=>"Listar nombre y edad de los ciclistas que han ganado puertos",
            "sql"=>"SELECT nombre,edad FROM(SELECT DISTINCT dorsal FROM puerto)c1 JOIN ciclista  USING(dorsal)",
        ]);
    }
    
    public function actionConsulta2a(){
        //mediante Active Record
        
         $dataProvider = new ActiveDataProvider([
             'query' => Ciclista::find()
                ->select("nombre,edad")->distinct()
                ->innerJoin('puerto','ciclista.dorsal = Puerto.dorsal')
         ]);

        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['nombre','edad'],
            "titulo"=>"Consulta 1.2 con Active Record",
            "enunciado"=>"Listar nombre y edad de los ciclistas que han ganado puertos",
            "sql"=>"SELECT nombre,edad FROM(SELECT DISTINCT dorsal FROM puerto)c1 JOIN ciclista  USING(dorsal)",
        ]);
    }
    
    
    public function actionConsulta3(){
        //mediante DAO
//        $numero = yii::$app->db
//                ->createCommand("SELECT count(distinct edad) From ciclista WHERE nomequipo ='Artiach' or nomequipo ='Amore Vita'")
//                ->queryScalar();
                
           $dataProvider = new SqlDataProvider([
            'sql' => "SELECT DISTINCT ciclista.nombre,ciclista.edad FROM(ciclista INNER JOIN etapa  ON ciclista.dorsal = etapa.dorsal)INNER JOIN puerto ON ciclista.dorsal = puerto.dorsal",
//            'totalCount' => $numero,
        ]);
        
        
        return $this->render('resultado',[
            "resultados"=>$dataProvider,
            "campos"=>['nombre','edad'],
            "titulo"=>"Consulta 1.3 con Dao",
            "enunciado"=>"Listar nombre y edad de los ciclistas que han ganado puertos y etapas",
            "sql"=>"SELECT DISTINCT ciclista.nombre,ciclista.edad FROM(ciclista INNER JOIN etapa  ON ciclista.dorsal = etapa.dorsal)INNER JOIN puerto ON ciclista.dorsal = puerto.dorsal",
        ]);
    }
    
    
    
    public function actionConsulta3a(){
        //mediante Active Record
        
         $dataProvider = new ActiveDataProvider([
             'query' => Ciclista::find()
                 ->select("ciclista.nombre,ciclista.edad")->distinct()
                 ->joinWith('etapas',false,'inner join')
                 ->joinWith('puertos',false,'inner join')
         ]);

        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['nombre','edad'],
            "titulo"=>"Consulta 1.3 con Active Record",
            "enunciado"=>"Listar nombre y edad de los ciclistas que han ganado puertos y etapas",
            "sql"=>"SELECT DISTINCT ciclista.nombre,ciclista.edad FROM(ciclista INNER JOIN etapa  ON ciclista.dorsal = etapa.dorsal)INNER JOIN puerto ON ciclista.dorsal = puerto.dorsal",
        ]);
    
      
    }
    
    
     public function actionConsulta4(){
        //mediante DAO
//        $numero = yii::$app->db
//                ->createCommand("SELECT count(distinct edad) From ciclista WHERE nomequipo ='Artiach' or nomequipo ='Amore Vita'")
//                ->queryScalar();
                
           $dataProvider = new SqlDataProvider([
            'sql' => "SELECT director FROM equipo WHERE nomequipo IN(
                        SELECT nomequipo FROM ciclista WHERE dorsal in(
                          SELECT DISTINCT dorsal FROM etapa))",
//            'totalCount' => $numero,
        ]);
        
        
        return $this->render('resultado',[
            "resultados"=>$dataProvider,
            "campos"=>['director'],
            "titulo"=>"Consulta 1.4 con Dao",
            "enunciado"=>"Director de los equipos que tengan ciclistas que hayan ganado alguna etapa",
            "sql"=>"SELECT director FROM equipo WHERE nomequipo IN(
                        SELECT nomequipo FROM ciclista WHERE dorsal in(
                            SELECT DISTINCT dorsal FROM etapa))",
        ]);
    }
    
    
    
    public function actionConsulta4a(){
        //mediante Active Record
        
         $dataProvider = new ActiveDataProvider([
             'query' => Equipo::find()
                ->select("director")->distinct()
                //->joinWith('etapa',false,'inner join')
                ->joinWith('ciclistas',false,'inner join')         
                ->innerJoin('etapa','ciclista.dorsal = etapa.dorsal')
         ]);

        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['director'],
            "titulo"=>"Consulta 1.4 con Active Record",
            "enunciado"=>"ciclistas que hayan ganado algun puerto",
            "sql"=>"SELECT DISTINCT dorsal FROM puerto"
        ]);
    
      
    }
        public function actionConsulta5(){
        
         //mediante DAO
        $numero = yii::$app->db
                ->createCommand("SELECT count(*) FROM ciclista WHERE dorsal IN(SELECT DISTINCT dorsal FROM lleva)")
                ->queryScalar();
                
           $dataProvider = new SqlDataProvider([
            'sql' => "SELECT dorsal,nombre FROM ciclista WHERE dorsal IN(SELECT DISTINCT dorsal FROM lleva)",
            'totalCount' => $numero,
            'pagination' => [
                'pageSize' =>5,
            ],   
        ]);
        
        
        return $this->render('resultado',[
            "resultados"=>$dataProvider,
            "campos"=>['dorsal','nombre'],
            "titulo"=>"Consulta 1.5 con Dao",
            "enunciado"=>"Dorsal y nombre de los ciclistas que han llevado algun maillot",
            "sql"=>"SELECT dorsal,nombre FROM ciclista WHERE dorsal IN(SELECT DISTINCT dorsal FROM lleva)",
            
        ]);
    
    }
    
    public function actionConsulta5a(){
        //mediante Active Record
        
         $dataProvider = new ActiveDataProvider([
             'query' => Ciclista::find()
                 ->select("ciclista.dorsal,nombre")->distinct()
                 //->innerJoin('lleva','ciclista.dorsal = lleva.dorsal'),
                 ->innerJoinWith('llevas'),
                 
             'pagination'=>[
                 'pageSize'=>5,
             ]
         ]);

        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['dorsal','nombre'],
            "titulo"=>"Consulta 1.5 con Active Record",
            "enunciado"=>"Dorsal y nombre de los ciclistas que han llevado algun maillot",
            "sql"=>"SELECT dorsal,nombre FROM ciclista WHERE dorsal IN(SELECT DISTINCT dorsal FROM lleva)",
        ]);
    
    }
     public function actionConsulta6(){
        //mediante Dao
         
        $numero = yii::$app->db
                ->createCommand("SELECT count(*) FROM ciclista WHERE dorsal IN(SELECT DISTINCT dorsal FROM lleva WHERE código ='MGE')")
                ->queryScalar();
                
           $dataProvider = new SqlDataProvider([
            'sql' => "SELECT dorsal,nombre FROM ciclista WHERE dorsal IN(SELECT DISTINCT dorsal FROM lleva WHERE código ='MGE')",
            'totalCount' => $numero,
            'pagination' => [
                'pageSize' =>10,
            ],   
        ]);
        
        
        return $this->render('resultado',[
            "resultados"=>$dataProvider,
            "campos"=>['dorsal','nombre'],
            "titulo"=>"Consulta 1.6 con Dao",
            "enunciado"=>"Listar Dorsal y nombre de los ciclistas que han llevado el maillot amarillo",
            "sql"=>"SELECT dorsal,nombre FROM ciclista WHERE dorsal IN(SELECT DISTINCT dorsal FROM lleva WHERE código ='MGE')",
        ]);
    
    }
    
    public function actionConsulta6a(){
        //mediante Active Record
        
         $dataProvider = new ActiveDataProvider([
             'query' => Ciclista::find()
                 ->select("ciclista.dorsal,nombre")->distinct()
                 //->innerJoin('lleva','ciclista.dorsal = lleva.dorsal'),
                 ->innerJoinWith('llevas')
                 ->where("código='MGE'"),
                
             'pagination'=>[
                 'pageSize'=>5,
             ]
         ]);

        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['dorsal','nombre'],
            "titulo"=>"Consulta 1.6 con Active Record",
            "enunciado"=>"Listar Dorsal y nombre de los ciclistas que han llevado el maillot amarillo",
            "sql"=>"SELECT dorsal,nombre FROM ciclista WHERE dorsal IN(SELECT DISTINCT dorsal FROM lleva WHERE código ='MGE')",
        ]);
    
    }
    public function actionConsulta7(){
        //mediante Dao
         
        $numero = yii::$app->db
                ->createCommand("SELECT count(*) FROM lleva JOIN etapa USING(dorsal);")
                ->queryScalar();
                
           $dataProvider = new SqlDataProvider([
            'sql' => "SELECT DISTINCT lleva.dorsal FROM lleva JOIN etapa USING(dorsal)",
            'totalCount' => $numero,
            'pagination' => [
                'pageSize' =>5,
            ],   
        ]);
        
        
        return $this->render('resultado',[
            "resultados"=>$dataProvider,
            "campos"=>['dorsal'],
            "titulo"=>"Consulta 1.7 con Dao",
            "enunciado"=>"Listar Dorsal de los ciclistas que han llevado algun maillot y que han ganado etapas",
            "sql"=>"SELECT DISTINCT lleva.dorsal FROM lleva JOIN etapa USING(dorsal)",
            
        ]);
    
    }
    
    public function actionConsulta7a(){
        //mediante Active Record
        
         $dataProvider = new ActiveDataProvider([
             'query' =>Lleva::find()
                 ->select("lleva.dorsal")
                 ->distinct()
                 //->innerJoinWith('ciclista'),
                 ->innerJoin('etapa','lleva.dorsal = etapa.dorsal'),
             'pagination'=>[
                 'pageSize'=>5,
             ]
         ]);

        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['dorsal'],
            "titulo"=>"Consulta 1.7 con Active Record",
            "enunciado"=>"Listar Dorsal de los ciclistas que han llevado algun maillot y que han ganado etapas",
            "sql"=>"SELECT DISTINCT lleva.dorsal FROM lleva JOIN etapa USING(dorsal)    ",
        ]);
    
    }
    
    
     public function actionConsulta8(){
        //mediante Dao
         
        $numero = yii::$app->db
                ->createCommand("SELECT distinct(count(*)) FROM puerto;")
                ->queryScalar();
                
           $dataProvider = new SqlDataProvider([
            'sql' => "SELECT DISTINCT numetapa FROM puerto",
            'totalCount' => $numero,
            'pagination' => [
                'pageSize' =>5,
            ],   
        ]);
        
        
        return $this->render('resultado',[
            "resultados"=>$dataProvider,
            "campos"=>['numetapa'],
            "titulo"=>"Consulta 1.8 con Dao",
            "enunciado"=>"Listar numetapa de las etapas que tengan puertos",
            "sql"=>"SELECT DISTINCT numetapa FROM puerto",
            
        ]);
    
    }
    
    public function actionConsulta8a(){
        //mediante Active Record
        
         $dataProvider = new ActiveDataProvider([
             'query' => Puerto::find()
              ->select('numetapa')->distinct(),
             'pagination'=>[
                 'pageSize'=>5,
             ]
         ]);

        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['numetapa'],
            "titulo"=>"Consulta 1.8 con Active Record",
            "enunciado"=>"Listar numetapa de las etapas que tengan puertos",
            "sql"=>"SELECT DISTINCT numetapa FROM puerto",
        ]);
    
    }
    public function actionConsulta9(){
        //mediante Dao
         
        $numero = yii::$app->db
                ->createCommand("SELECT count(*) FROM etapa JOIN puerto USING(numetapa) WHERE numetapa IN(
                                    SELECT DISTINCT numetapa FROM etapa WHERE dorsal IN(
                                      SELECT DISTINCT dorsal FROM ciclista WHERE nomequipo = 'Banesto'
                                    ))")
                ->queryScalar();
                
           $dataProvider = new SqlDataProvider([
            'sql' => "SELECT DISTINCT numetapa,kms FROM etapa JOIN puerto USING(numetapa) WHERE numetapa IN(
                            SELECT DISTINCT numetapa FROM etapa WHERE dorsal IN(
                            SELECT DISTINCT dorsal FROM ciclista WHERE nomequipo = 'Banesto'
                            ))",
            'totalCount' => $numero,
            'pagination' => [
                'pageSize' =>5,
            ],   
        ]);
        
        
        return $this->render('resultado',[
            "resultados"=>$dataProvider,
            "campos"=>['kms','numetapa'],
            "titulo"=>"Consulta 1.9 con Dao",
            "enunciado"=>"Listar numetapa de las etapas que tengan puertos",
            "sql"=>"SELECT DISTINCT numetapa,kms FROM etapa JOIN puerto USING(numetapa) WHERE numetapa IN(
                            SELECT DISTINCT numetapa FROM etapa WHERE dorsal IN(
                             SELECT DISTINCT dorsal FROM ciclista WHERE nomequipo = 'Banesto'))",
        ]);
    
    }
    
    public function actionConsulta9a(){
        //mediante Active Record
        
         $dataProvider = new ActiveDataProvider([
             'query' => Etapa::find()
              ->select('kms,etapa.numetapa')
              ->innerJoinWith('puertos')
              ->innerJoinWith('dorsal0')
              ->where("nomequipo = 'Banesto'"),
             'pagination'=>[
                 'pageSize'=>5,
             ]
         ]);

        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['kms','numetapa'],
            "titulo"=>"Consulta 1.9 con Active Record",
            "enunciado"=>"Listar kms de las etapas que hayan ganado ciclistas de Banesto y con puertos",
            "sql"=>"SELECT DISTINCT numetapa,kms FROM etapa JOIN puerto USING(numetapa) WHERE numetapa IN(
                      SELECT DISTINCT numetapa FROM etapa WHERE dorsal IN(
                      SELECT DISTINCT dorsal FROM ciclista WHERE nomequipo = 'Banesto'))",
        ]);
    
    }
    
    public function actionConsulta10(){
        //mediante Dao
         
        $numero = yii::$app->db
                ->createCommand("SELECT COUNT(*)FROM(
                             SELECT DISTINCT etapa.dorsal FROM etapa inner JOIN puerto USING(numetapa))c1")
                ->queryScalar();
                
           $dataProvider = new SqlDataProvider([
            'sql' => "SELECT COUNT(*)total FROM(SELECT DISTINCT etapa.dorsal FROM etapa inner JOIN puerto USING(numetapa))c1",
            'totalCount' => $numero,
            'pagination' => [
                'pageSize' =>5,
            ],   
        ]);
        
        
        return $this->render('resultado',[
            "resultados"=>$dataProvider,
            "campos"=>['total'],
            "titulo"=>"Consulta 1.10 con Dao",
            "enunciado"=>"Listar total ciclistas que hayan ganado alguna etapa con puerto",
            "sql"=>"SELECT COUNT(*)total FROM(SELECT DISTINCT etapa.dorsal FROM etapa inner JOIN puerto USING(numetapa))c1",
            
        ]);
    
    }
    
    public function actionConsulta10a(){
        //mediante Active Record
        
         $dataProvider = new ActiveDataProvider([
             'query' => Etapa::find()
               ->distinct()
               ->select('count(distinct etapa.dorsal) total')
               //->innerJoin('puerto','puerto.numetapa = etapa.numetapa') 
               ->innerJoinWith('puertos'),
             'pagination'=>[
                 'pageSize'=>5,
             ]
         ]);

        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['total'],
            "titulo"=>"Consulta 1.10 con Active Record",
            "enunciado"=>"Listar total ciclistas que hayan ganado alguna etapa con puerto",
            "sql"=>"SELECT COUNT(*)total FROM(SELECT DISTINCT etapa.dorsal FROM etapa inner JOIN puerto USING(numetapa))c1",
        ]);
    
    }
    public function actionConsulta11(){
        //mediante Dao
         
        $numero = yii::$app->db
                ->createCommand("SELECT DISTINCT nompuerto FROM puerto WHERE dorsal IN(SELECT DISTINCT dorsal FROM ciclista WHERE nomequipo = 'Banesto')")
                ->queryScalar();
                
           $dataProvider = new SqlDataProvider([
            'sql' => "SELECT DISTINCT nompuerto FROM puerto WHERE dorsal IN(SELECT DISTINCT dorsal FROM ciclista WHERE nomequipo = 'Banesto')",
            'totalCount' => $numero,
            'pagination' => [
                'pageSize' =>5,
            ],   
        ]);
        
        
        return $this->render('resultado',[
            "resultados"=>$dataProvider,
            "campos"=>['nompuerto'],
            "titulo"=>"Consulta 1.11 con Dao",
            "enunciado"=>"Nombre de los puertos ganados por ciclistas de banesto",
            "sql"=>"SELECT DISTINCT nompuerto FROM puerto WHERE dorsal IN(SELECT DISTINCT dorsal FROM ciclista WHERE nomequipo = 'Banesto')",
            
        ]);
    
    }
    
    public function actionConsulta11a(){
        //mediante Active Record
        
         $dataProvider = new ActiveDataProvider([
             'query' => Puerto::find()
              ->select('nompuerto')
              ->innerJoinWith('dorsal0')
              ->where("nomequipo = 'Banesto'"),
             
             'pagination'=>[
                 'pageSize'=>5,
             ]
         ]);

        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['nompuerto'],
            "titulo"=>"Consulta 1.11 con Active Record",
            "enunciado"=>"Nombre de los puertos ganados por ciclistas de banesto",
            "sql"=>"SELECT DISTINCT nompuerto FROM puerto WHERE dorsal IN(SELECT DISTINCT dorsal FROM ciclista WHERE nomequipo = 'Banesto')",
        ]);
    
    }
    
     public function actionConsulta12(){
        //mediante Dao
         
        $numero = yii::$app->db
                ->createCommand("SELECT count(*) FROM(
                                    SELECT * FROM ciclista  JOIN etapa USING(dorsal) WHERE nomequipo = 'Banesto' AND kms >=200
                                        )c1 JOIN puerto USING(numetapa)")
                ->queryScalar();
                
           $dataProvider = new SqlDataProvider([
            'sql' => "SELECT DISTINCT c1.numetapa FROM(
                                SELECT * FROM ciclista  JOIN etapa USING(dorsal) WHERE nomequipo = 'Banesto' AND kms >=200
                              )c1 JOIN puerto USING(numetapa)",
            'totalCount' => $numero,
            'pagination' => [
                'pageSize' =>5,
            ],   
        ]);
        
        
        return $this->render('resultado',[
            "resultados"=>$dataProvider,
            "campos"=>['numetapa'],
            "titulo"=>"Consulta 1.12 con Dao",
            "enunciado"=>"Numero de etapas que tengan puerto ganados por ciclistas de banesto con mas de 200 kms",
            "sql"=>"SELECT DISTINCT c1.numetapa FROM(
                        SELECT * FROM ciclista  JOIN etapa USING(dorsal) WHERE nomequipo = 'Banesto' AND kms >=200
                      )c1 JOIN puerto USING(numetapa)",

        ]);
    
    }
    
    public function actionConsulta12a(){
        //mediante Active Record
        
         $dataProvider = new ActiveDataProvider([
             'query' => Etapa::find()
              ->select('etapa.numetapa')->distinct()
              ->innerJoinWith('dorsal0')
              //->innerJoin('etapa','etapa.dorsal = ciclista.dorsal')  
              ->innerJoin('puerto','puerto.numetapa = etapa.numetapa')
              ->where("ciclista.nomequipo = 'Banesto' and etapa.kms >= 200"),
             
             'pagination'=>[
                 'pageSize'=>5,
             ]
         ]);

        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['numetapa'],
            "titulo"=>"Consulta 1.12 con Active Record",
            "enunciado"=>"Numero de etapas que tengan puerto ganados por ciclistas de banesto con mas de 200 kms",
            "sql"=>"SELECT DISTINCT c1.numetapa FROM(
                                SELECT DISTINCT * FROM ciclista  JOIN etapa USING(dorsal) WHERE nomequipo = 'Banesto' AND kms >=200
                              )c1 JOIN puerto USING(numetapa)",
        ]);
    
    }
    
    
    
}
